import React, { Component } from 'react';
import './App.css';
import Layout from "./component/Loyout/Loyout";
import {Route} from "react-router";
import AddFormContact from "./container/AddFormContact.js/AddFormContact";
import Contacts from "./container/Contacts/Contacts";
import EditContacts from "./container/EditContacts/EditContacts";



class App extends Component {
  render() {
    return (

          <Layout>
              <Route path="/" exact component={Contacts}/>

            <Route path="/add/contacts" component={AddFormContact}/>
              <Route path="/edit/contacts" component={EditContacts}/>

          </Layout>
    );
  }
}

export default App;
