import React, {Fragment} from 'react';

import './ContactInfo.css'
const ContactInfo = (props) => {
    return (

        <div>

                {props.info &&
                <Fragment>
                <img src={props.info.photo}  alt="imge"/>
                <div className="display">
                    <h5>{props.info.name}</h5>
                    <h5>tel :{props.info.number}</h5>
                    <h5>mail :{props.info.email}</h5>
                </div>
                    <div>
                        <button onClick={() => props.edit()}>edit</button>
                        <button onClick={() => props.remove(props.info.id)}>delete</button>
                    </div>


            </Fragment>

                }

        </div>
    );
};

export default ContactInfo;