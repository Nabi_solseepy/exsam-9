import React from 'react';
import {Button, Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavLink,} from "reactstrap";

import {NavLink as RouterNavLink} from "react-router-dom";

const Header = () => {
    return (
        <div>
            <Navbar color="secondary" light expand="md">
                <NavbarBrand href="/"><h1>Contact</h1></NavbarBrand>
                <NavbarToggler  />
                <Collapse  navbar>
                    <Nav className="ml-auto" navbar>
                    </Nav>
                    <NavLink tag={RouterNavLink} to="/add/contacts"> <Button color="info">Add new contact</Button></NavLink>
                </Collapse>
            </Navbar>
        </div>
    );
};

export default Header;