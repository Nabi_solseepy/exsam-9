import React, {Component} from 'react';
import {closeModal, contactGet, deleteInfoContact, infoContacts, } from "../../store/action";
import {connect} from "react-redux";
import { Card, CardBody, CardColumns, CardImg, CardTitle} from "reactstrap";

import ContactInfo from "../../component/ContactInfo/ContactInfo";
import Modal from "../../component/UI/Modal/Modal";

class Contacts extends Component {
    componentDidMount() {
        this.props.contactGet()
    }


    editContinue = () => {
       this.props.history.push('/edit/contacts')
    };

    render() {
        return (
            <CardColumns>
              <Modal close={this.props.closeModal} show={this.props.show}>
                   <ContactInfo edit={this.editContinue} remove={this.props.deleteInfoContact} info={this.props.info}/>
              </Modal>
                {this.props.contacts.map((contact,id) => {
                    return(
                            <Card key={id} onClick={() => this.props.infoContacts(contact.id)}  >
                                <CardImg src={contact.photo} />
                                <CardBody>
                                    <CardTitle>{contact.name}</CardTitle>

                                </CardBody>
                            </Card>
                    )
                })}

            </CardColumns>
        );
    }
}

const mapStateToProps = state =>({
    contacts: state.contacts,
    info: state.info,
    show: state.show
});


const mapDispachToProps = dispatch => ({
    contactGet: () => dispatch(contactGet()),
    infoContacts: id => dispatch(infoContacts(id)),
    closeModal: () => dispatch(closeModal()),
    deleteInfoContact : (id) => dispatch(deleteInfoContact(id))


});

export default connect(mapStateToProps, mapDispachToProps)(Contacts);