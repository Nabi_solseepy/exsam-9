import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class EditContacts extends Component {

    state = {
        name: '',
        email: '',
        number: '',
        photo: '',

    };

    valueContacts = (event) => {
        const  name = event.target.name;
        this.setState({[name]: event.target.value})
    };
    render() {
        return (
            <div>
                <Form className='pt-5'>
                    <FormGroup mt={5} row>
                        <Label for="exampleEmail" sm={2}>Name</Label>
                        <Col sm={10}>
                            <Input type="name" onChange={this.valueContacts}  value={this.state.name}  name="name" id="exampleEmail"/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="examplePassword" sm={2}>Phone</Label>
                        <Col sm={10}>
                            <Input type="number" onChange={this.valueContacts}   value={this.state.number}  name="number" id="examplePassword"  />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="examplePassword" sm={2}>Email</Label>
                        <Col sm={10}>
                            <Input type="email" onChange={this.valueContacts}   value={this.state.email}  name="email" id="examplePassword"  />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="examplePassword" sm={2}>Photo</Label>
                        <Col sm={10}>
                            <Input type="photo"  onChange={this.valueContacts}  value={this.state.photo}  name="photo" id="examplePassword"  />
                        </Col>
                    </FormGroup>

                    <Button color="danger" onClick={this.handlerContacts}>save</Button>
                    <Button  className='ml-5' color="danger">Back to contacts</Button>
                </Form>
            </div>
        );
    }
}

export default EditContacts;