import  axios from  '../axios.contacts'

export const POST_CPNTACT_REQUEST = ' POST_CPNTACTS_REQUEST';
export const POST_CONTACT_SUCCESS = 'POST_CONTACT_SUCCESSS';


export const   RECEIVE_CONTACT_REQUEST = 'RECEIVE_CONTACT_REQUEST';
export const   RECEIVE_CONTACT_SUCCESS = 'RECEIVE_CONTACT_SUCCESS';
export const   INFO_CONTACT = ' INFO_CONTACT';


export const REMOVE_CONTACT_REQUEST = 'REMOVE_CONTACT_REQUEST';

export const MODAL_CLOSE = 'MODAL_CLOSE';

export const closeModal = () => ({type: MODAL_CLOSE});

export const infoContact = (contact) => ({type: INFO_CONTACT, contact});

export  const  postToContact = () => ({type: POST_CPNTACT_REQUEST});

export const recieveToContact = (contact) => ({type: RECEIVE_CONTACT_SUCCESS, contact});


export const removeInfoContacts = () => ({type: REMOVE_CONTACT_REQUEST});

export const contactGet = () => {
  return dispatch => {
      axios.get('Contact.json').then(response => {

          const contacts = Object.keys(response.data).map(id => {
              return {...response.data[id], id}
          });
          dispatch(recieveToContact(contacts))
      })
  }
};




export  const  contactPosts = (contactsData, history) => {
    return dispatch => {
        axios.post('Contact.json', contactsData).then(response => {
            history.push('/')
        })
    }
};

export const  infoContacts = (id) => {
     return dispatch => {
         axios.get('Contact/'+ id + '.json').then(result => {
             dispatch(infoContact({...result.data, id}))
         })
     }
};



export const closedmodal = () => {
    return dispatch => {
        dispatch(closeModal)
    }
};


export const deleteInfoContact = id => {
    return dispatch => {
        axios.delete("Contact/" + id + ".json").then(() => {
            dispatch(removeInfoContacts());
            dispatch(contactGet())
        })
    }
};

