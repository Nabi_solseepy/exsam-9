import {INFO_CONTACT, MODAL_CLOSE, RECEIVE_CONTACT_SUCCESS, REMOVE_CONTACT_REQUEST} from "./action";


const initialState = {
   contacts: [],
    info: null,
    show: false
};

const contactReducer = (state = initialState, action) => {
    switch (action.type) {
        case RECEIVE_CONTACT_SUCCESS:
            return {
                ...state,
                contacts: action.contact
            };
        case INFO_CONTACT:
            console.log(action);
            return{
                ...state,
                info: {...action.contact},
                show: true
            };
        case  MODAL_CLOSE:
            return{
                ...state,
                show: false
            };
        case REMOVE_CONTACT_REQUEST:
            return{
                ...state,
                show: false
            };

        default: return state
    }
};

export default  contactReducer;